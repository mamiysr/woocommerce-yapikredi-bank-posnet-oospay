<?php
/*
Plugin Name: Yapı Kredi OOSPay - WooCommerce Gateway
Plugin URI: http://mamiysr.wordpress.com/
Description: Yapı Kredi Bankası Ortak Ödeme Sayfası WooCommerce Eklentisi.
Version: 1.1
Author: Dev by Çağdaş Takış | Core and Contr. by mamiysr (Işbara Han)
Author URI: http://yorci.com/
*/
 

add_action( 'plugins_loaded', 'init_yapi_kredi_oos_gateway', 0 );
function init_yapi_kredi_oos_gateway() {
   
    if ( ! class_exists( 'WC_Payment_Gateway' ) ) return;
     
    include_once( 'WC_Gateway_YapiKredi_OOS.php' );
 
    function add_yapi_kredi_oos_gateway( $methods ) {
        $methods[] = 'wc_gateway_yapi_kredi_oos';
        return $methods;
    }
    add_filter( 'woocommerce_payment_gateways', 'add_yapi_kredi_oos_gateway' );
}
 
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'ysr_yapi_kredi_oos_action_links' );
function ysr_yapi_kredi_oos_action_links( $links ) {
    $plugin_links = array(
        '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout' ) . '">' . __( 'Ayarlar', 'ysr_yapi_kredi_oos' ) . '</a>',
    );
     return array_merge( $plugin_links, $links );    
}