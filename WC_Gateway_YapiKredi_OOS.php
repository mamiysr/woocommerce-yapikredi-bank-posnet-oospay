<?php 

/**
* 
*/

class WC_Gateway_Yapi_Kredi_OOS extends WC_Payment_Gateway{

	public $Type = "Sale";
	public $CurrencyCode = "TL";
	public $Lang = "tr";
	public $liveurl = 'https://www.posnet.ykb.com/3DSWebService/OOS';
	public $testurl = 'http://setmpos.ykb.com/3DSWebService/OOS';

	function __construct(){
		$this->id = "yorci_yapikredi_oos";
		$this->method_title = __( "Yapi Kredi Bank OOSPay", 'yorci_yapikredi_oos' );
		$this->method_description = __( "WooCommerce için Yapı Kredi Bankası Ortak Ödeme Sayfası", 'yorci_yapikredi_oos' );
		$this->title = __( "Yapı Kredi OOSPay", 'yorci_yapikredi_oos' );
		$this->notify_url = WC()->api_request_url( 'WC_Gateway_Yapi_Kredi_OOS' );

		$this->icon = NULL;
		$this->has_fields = true;
		$this->init_form_fields();
		$this->init_settings();

		$this->posnet_IDm = $this->get_option('posnet_IDm');
		$this->terminalID = $this->get_option('terminalID');
		$this->merchantID = $this->get_option('merchantID');
		$this->description = $this->get_option( 'description' );
		$this->test_ortami = $this->get_option('test_ortami');

		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action(	'woocommerce_receipt_yorci_yapikredi_oos', array( $this, 'receipt_page' ));
		add_action( 'Yapi_KredibankCallback', array( $this, 'gelen_veri' ) );
		add_action('woocommerce_api_wc_gateway_yapi_kredi_oos', array( $this, 'handle_callback' ));
	}
	

	public function init_form_fields() {
		$this->form_fields = array(
			'enabled' => array(
				'title'   => __( 'Enable/Disable', 'woocommerce' ),
				'type'    => 'checkbox',
				'label'   => __( 'Yapı Kredi OOSPay Etkin', 'woocommerce' ),
				'default' => 'yes'
				),
			'test_ortami' => array(
				'title' => __( 'Test Modu?', 'yorci_yapikredi_oos' ),
				'type' => 'checkbox',
				'label' => __( 'Test Modu Açık mı?', 'yorci_yapikredi_oos' ),
				'default' => 'yes'
				),
			'description' => array(
				'title'       => __( 'Description', 'woocommerce' ),
				'type'        => 'text',
				'desc_tip'    => true,
				'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce' ),
				'default'     => __( 'Yapı Kredi Ortak Ödeme Sayfası ile güvenli ödeme sunar.', 'woocommerce' )
				),
			'posnet_IDm' => array(
				'title' => __( 'PosNet No', 'yorci_yapikredi_oos' ),
				'type' => 'text',
				'description' => __( 'Faturada Görünecek Firma Adı.', 'yorci_yapikredi_oos' ),
				'default' => __( '000', 'yorci_yapikredi_oos' ),
				'desc_tip'      => true,
				),
			'terminalID' => array(
				'title' => __( 'Terminal no (TID)', 'yorci_yapikredi_oos' ),
				'type' => 'text',
				'description' => __( 'Bankanın Size Sağladığı 8 Karakterli Terminal ID.', 'yorci_yapikredi_oos' ),
				'default' => __( '000', 'yorci_yapikredi_oos' ),
				'desc_tip'      => true,
				),
			'merchantID' => array(
				'title' => __( 'Üye işyeri no (MID)', 'yorci_yapikredi_oos' ),
				'type' => 'text',
				'description' => __( 'Bankanın Size Sağladığı 7 Karakterli Merchant ID.', 'yorci_yapikredi_oos' ),
				'default' => __( '7000679', 'yorci_yapikredi_oos' ),
				'desc_tip'      => true,
				),
			);
	}

	public function receipt_page($order){
		echo '<div class="alert alert-info" role="alert">'.__('Siparişiniz için teşekkür ederiz, Yapı Kredi OOSPay ile ödeme için aşağıdaki butona tıklayınız. Bu sizi bilgilerinizi güvenli şekilde girmeniz için bankaya yönlendirecektir, ödeme işleminden hemen sonra tekrar sitemize döneceksiniz.', 'yorci_yapikredi_oos').'</div>';
		echo $this->veri_getir($order);
	}

	public function veri_getir( $order_id ) {
		global $woocommerce;
		$mus_siparis = new WC_Order($order_id);

		$order_total = $mus_siparis->order_total;
		$pos_order_id = $mus_siparis->id;
		$pos_order_idsi = sprintf("%020d", $pos_order_id);


		$postparams = array(
			'terminalid' => $this->terminalID,
			'mid' => $this->merchantID,
			'xid' => $pos_order_idsi,
			'amount' => $order_total,
			'currencyCode' => 'TL',
			'posnetID' => $this->posnet_IDm,
			'tranType' => 'Sale',
			'merchantReturnSuccessURL' => $this->notify_url,
			'merchantReturnFailURL' => $this->notify_url,
			'lang' => $this->Lang,
			);

		$Yapi_Kredi_args = array();
		foreach($postparams as $key => $value){
			$Yapi_Kredi_args[] = "<input type='hidden' name='".$key."' value='".$value."'/>";
		}

		if($this->test_ortami == 'yes'){
			$processURI = $this->testurl;
		}
		else{
			$processURI = $this->liveurl;
		}

		$html_form    =
		'<form action="'.$processURI.'" method="post" id="Yapi_Kredi_oos_payment_form">' 
		.implode('', $Yapi_Kredi_args) 
		.'<input type="submit" class="button alt" id="submit_Yapi_Kredi_oos_payment_form" value="'.__('Yapı Kredi OOSPay ile güvenli öde', 'yorci_yapikredi_oos').'" /> <a class="button alt cancel" href="'.$mus_siparis->get_cancel_order_url().'">'.__('Vazgeç ve sepeti geri getir', 'yorci_yapikredi_oos').'</a>'
		.'</form>';

		return $html_form;
	}

	public function handle_callback(){
		@ob_clean();
		if($_GET){
			$callback = stripslashes_deep($_GET);
			do_action( "Yapi_KredibankCallback", $callback );
		}else{
			$geri = '<a href="'.$woocommerce->cart->get_checkout_url().'" class="button alt" role="button">« Sipariş sayfasına geri dön</a>';
			wp_die( "Banka'dan veri alınamadı. Lütfen site yöneticileriyle iletişime geçin.<br>".$geri, "Yapı Kredi Callback", array( 'response' => 200 ) );
		}
	}

	public function gelen_veri(){
		global $woocommerce;

		if($_GET['returncode'] === "1")
		{

			$order_id = ltrim($_GET['xid'], '0');
			//sipariş bilgileri
			$order = wc_get_order($order_id);
			//sipariş tutarı
			$order_total = str_replace(array('.', ','), '' , $order->order_total);
			$order_get_price = str_replace(array('.', ','), '' , $_GET['amount']);
			
			//Eğer sipariş tutarıyla bankanın çektiği ücret aynıysa.
			if( (100*$order_get_price) == $order_total){
				$order->update_status('on-hold', __( 'Yapı Kredi Bank OOS ödemesi başarılı,', 'yorci_yapikredi_oos' ));
				$order->add_order_note('YKB Referans No: '.$_GET['ykbrefno']);
				$woocommerce->cart->empty_cart();
				$order->payment_complete();
				wp_redirect($this->get_return_url($order));
				exit;
			}else{
				$geri = '<a href="'.$woocommerce->cart->get_checkout_url().'" class="button alt" role="button">« Sipariş sayfasına geri dön</a>';
				wp_die('Ödenen tutar sipariş tutarına eşit değil! <br>'.$geri,'HATA!',array('response' => 200));
			}
		}elseif($_GET['returncode'] === "2"){
			$geri = '<a href="'.$woocommerce->cart->get_checkout_url().'" class="button alt" role="button">« Sipariş sayfasına geri dön</a>';
			wp_die('Zaten daha önce onaylanmış! <br>'.$geri,'HATA!',array('response' => 200));
		}else{
			$errmsg = $_GET['errmsg'];
			$mderrmsg = $_GET['returnmessage'];
			$hostmsg = $_GET['hostmsg'];
			$geri = '<a href="'.$woocommerce->cart->get_checkout_url().'" class="button alt" role="button">« Sipariş sayfasına geri dön</a>';
			wp_die($mderrmsg.'<br>'.$errmsg.'<br>'.$hostmsg.'<br>'.$geri,'HATA!',array('response' => 200));
		}

	}

	public function process_payment( $order_id ) {
		$order = wc_get_order($order_id);

		return array(
			'result'  => 'success',
			'redirect'  => $order->get_checkout_payment_url( true )
			);
	}

}
